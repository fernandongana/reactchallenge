/* eslint-disable prettier/prettier */
import { RECEIVE_API_DATA, RECEIVE_API_ERROR, PROGRESS} from '../actions/actions';
import { Todo } from 'src/types/Todo';

const todoReducer = (state = {}, action: { type: any; progress: any; todo: Todo; err: any; }) => {
  switch (action.type) {
    case PROGRESS:
      return {...state, progress: action.progress};
    case RECEIVE_API_DATA:
      return {...state, todo: action.todo};
    case RECEIVE_API_ERROR:
      return {err: action.err};

    default:
      return state;
  }
};

export default todoReducer;
