/* eslint-disable prettier/prettier */
// Imports: Dependencies
import { combineReducers } from 'redux';

// Imports: Reducers
import todoReducer from './todoReducer';

// Redux: Root Reducer
const rootReducer = combineReducers({
//  todo: todoReducer,
  todo: todoReducer,
});

// Exports
export default rootReducer;
