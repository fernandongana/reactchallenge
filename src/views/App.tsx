/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
// Imports: Dependencies
import React from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// Imports: Screens
import Home from './home/HomeScreen';
import Tasks from './tasks/TasksScreen';

// Imports: Redux Store
import { store } from '../stores/store';
import { StatusBar } from 'react-native';

// React Native App
export default function App() {

  const Stack = createStackNavigator();

  return (
    // Redux: Global Store
    <Provider store={store}>
      <NavigationContainer>
      <StatusBar barStyle="light-content" backgroundColor="#065965"/>
        <Stack.Navigator>
          <Stack.Screen name="Home" options={{ title: 'TODO LIST' }} component={Home}/>
          <Stack.Screen name="Tasks" options={{ title: 'TODO LIST ' }} component={Tasks}/>
        </Stack.Navigator> 
    </NavigationContainer>
    </Provider>
  );
}
