/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
// Imports: Dependencies
import React from 'react';
import { SafeAreaView, Text, View, ScrollView, CheckBox } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { requestApiData, REQUEST_API_DATA } from '../../actions/actions';
import { globalStyles } from '../styles/globalStyle';
import { Todo } from 'src/types/Todo';


interface Props {
  requestApiData: (action: any) => {};
  todos: Todo[];
  progress: number;
  error: any;
}

// Screen: Tasks Screen
class Tasks extends React.Component<Props> {

  componentDidMount() {
    this.props.requestApiData(REQUEST_API_DATA);
  }

  //Function to load todo Tasks
  todoView(todos: Todo[]){
    return todos.map((todo: Todo) => {  
        return (       
          <Text key={todo.id} style={{fontSize: 20, color: '#065965', paddingTop: 15, paddingLeft: 5,
            textDecorationLine: todo.completed ? 'line-through' : 'none'}}>
            
            <CheckBox style={globalStyles.combobox} 
              value={todo.completed}
              onValueChange={() => 
                console.log('on Change')
                }
              />   
            {todo.title}
          </Text>                 
        );
    });
  }

  //Function to load Progress indicator 
  loadingProgressView(data: any){
    return (
        <View style={globalStyles.counterContainer}>
                <Text style={globalStyles.title}>Carregando...!</Text>
                    <Text style={{fontSize: 40, color: '#065965', paddingTop: 10}}>
                        { data ? data + '%' : '0%'}
                    </Text>
        </View>
    );
  }

  //Function to load errors
  loadError(error: any){
    return (
      <View>
        <Text style={{fontSize: 30, color: 'red', paddingTop: 10}}>{error}</Text>
      </View>
    );
  }

  render() {    
    return (
      <SafeAreaView style={globalStyles.container}>      
        <ScrollView>
            <View style={globalStyles.counterContainer}>
               
                <View>
                    <Text style={globalStyles.title}>{this.props.todos ? 'My Tasks' : ''}</Text>
                    { //Ternary Operator to load views
                      (!this.props.progress) && (this.props.error) ? this.loadError(this.props.error) 
                      : (this.props.todos) ? this.todoView(this.props.todos) 
                      : this.loadingProgressView(this.props.progress) 
                    } 
                </View>

            </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}


// Map State To Props (Redux Store Passes State To Component)
const mapStateToProps = (state: any) => {
  // Redux Store --> Component
  return {
    progress: state.todo.progress,
    todos: state.todo.todo,
    error: state.todo.err,
  };
};

// Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
const mapDispatchToProps = (dispatch : Dispatch) => {
  return bindActionCreators({requestApiData : requestApiData} , dispatch);
  
};

// Exports
export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
