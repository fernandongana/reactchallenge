/* eslint-disable prettier/prettier */
import { StyleSheet} from 'react-native';

export const  globalStyles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: 10,
    },
    combobox: {
      width: 40,
      height: 10,
      color: '#065965',
    },
    counterContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 20,
      paddingBottom: 30,
    },
    title: {
      fontFamily: 'System',
      fontSize: 32,
      fontWeight: '700',
      color: '#065965',
    },
    mainTitle: {
        fontFamily: 'System',
        fontSize: 86,
        fontWeight: 'bold',
        color: '#065965',
    },
    buttonContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 150,
    },
    button: {
        backgroundColor: '#065965',
        borderRadius   : 10,
        width: 160,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonLabel: {
      color:'#ffff',
      fontSize: 24,
    },

  });

