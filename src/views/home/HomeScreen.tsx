/* eslint-disable prettier/prettier */
import React from 'react';
import { SafeAreaView, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { globalStyles } from '../styles/globalStyle';

interface Props {
  navigation: any
}

// Screen: Home
class Home extends React.Component<Props> {

  navigateToPage = (page: any)=>{
    this.props.navigation.navigate(page);
  };

  render() {
    return (
      <SafeAreaView style={globalStyles.container}>
        <ScrollView>
          <View style={globalStyles.counterContainer}>
            <Text style={globalStyles.mainTitle}>Todo</Text>
            <Text style={globalStyles.mainTitle}>List!</Text>
            <View style={globalStyles.buttonContainer}>
              <TouchableOpacity style={globalStyles.button}
                onPress={() => this.navigateToPage('Tasks') }>
              <Text style={globalStyles.buttonLabel}>Próximo</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Home;
