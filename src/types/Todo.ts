/* eslint-disable prettier/prettier */
export interface Todo{
    userId: number;
    id: number;
    title: string;
    completed: boolean;
}
