/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import axios from 'axios';

const baseUrl = 'https://jsonplaceholder.typicode.com';

//Function to fetch api data with onDownloadProgress as parameter 
export const  getTodos = (onProgress: (event: any) => void) => {
  const urlTodos = baseUrl + '/todos';

  const config = {
    onDownloadProgress: onProgress,
    withCredentials: true,
  };

  return axios.get(urlTodos, config);
};



