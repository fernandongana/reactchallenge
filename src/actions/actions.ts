/* eslint-disable prettier/prettier */
/* eslint-disable quotes */

import { Todo } from "src/types/Todo";



export const REQUEST_API_DATA = "REQUEST_API_DATA";
export const RECEIVE_API_DATA = "RECEIVE_API_DATA";
export const RECEIVE_API_ERROR = "RECEIVE_API_ERROR";
export const PROGRESS = "PROGRESS";

export const requestApiData = () => ({ type: REQUEST_API_DATA });
export const receiveApiData = (todo: Todo) => ({ type: RECEIVE_API_DATA, todo });
export const receiveApiError = (err: any) => ({type: RECEIVE_API_ERROR, err});
export const progressApi = (progress: any) => ({type: PROGRESS, progress});
