/* eslint-disable prettier/prettier */
// Imports: Dependencies
import { all, fork } from 'redux-saga/effects';

// Imports: Redux Sagas
import { watchTodos} from './todoSaga';

// Redux Saga: Root Saga
export function* rootSaga () {
  yield all([
    fork(watchTodos),
  ]);
}
