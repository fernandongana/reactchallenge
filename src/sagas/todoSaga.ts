/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
import { call, put, takeLatest, take, fork } from "redux-saga/effects";
import {eventChannel, END} from 'redux-saga';
import { receiveApiData, REQUEST_API_DATA, receiveApiError, progressApi } from "../actions/actions";
import {getTodos} from "../services/api";

function createGetter() {

  let emit;
  const chan = eventChannel(emitter => {

    emit = emitter;
    return () => {}; // return unsubscribe function. In our case 
                     // it's empty function
  });

  const downoadPromise = getTodos((event) => {
    //if download event is 
    if (event.target._response.length === 1) {
      emit(END);
    }
    //let total = progressEvent.target._response('content-length'); 
      
    //the API doesnt return content-length, that's why i use fake_total length - (https://jsonplaceholder.typicode.com/todos).

    let fake_total = 24311;
    let loaded = event.target._response.length;

    let percentCompleted = Math.round((loaded * 100) / fake_total);
    emit(percentCompleted);
  });

  return [ downoadPromise, chan ];
}

function* watchOnProgress(chan: any) {
  while (true){
    const percent = yield take(chan);
    yield put(progressApi(percent));
  }
}

function* getApiData(_action: any) { 
  const [ getPromise, chan ] = createGetter();
   // do api progress call
  yield fork(watchOnProgress, chan);

  try {
    // do api call
    const response = yield call(() => getPromise);
    yield put(receiveApiData(response.data));
  } catch (e) {
    //Pass api errors/ network errors
    yield put(receiveApiError(JSON.stringify(e.message)));
  }
}

export function* watchTodos() {
  yield takeLatest(REQUEST_API_DATA, getApiData);
}
